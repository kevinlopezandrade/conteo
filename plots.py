import os
import socket
import sys
import math
import numpy as np
import pandas as pd
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import pickle

from collections import OrderedDict
from algorithms import Algorithm


class PlotInstruction:

    def run(self, X:np.array, G:np.array):
        raise NotImplementedError


    @staticmethod
    def instance_algorithm(instruction_params_dict) -> Algorithm:
        algorithm_name = instruction_params_dict["algoritmo"]
        algorithm_params = instruction_params_dict.get("algoritmo_parametros", None)

        import parse

        algorithm_cls = parse.ALGORITMOS[algorithm_name]
        algorithm = algorithm_cls.from_config(algorithm_params)

        return algorithm


    @classmethod
    def from_config(cls, algorithm_params_dict):
        raise NotImplementedError


class PlotAlgorithmCummulated(PlotInstruction):

    def __init__(self, algorithm: Algorithm, start_pos:int, col_window: int, ticks_each: int):
        self.algorithm = algorithm
        self.start_pos = start_pos
        self.col_window = col_window
        self.ticks_each = ticks_each

    @classmethod
    def from_config(cls, instruction_params_dict):
        start_pos = instruction_params_dict.get("posicion", None)
        col_window = instruction_params_dict.get("columna", None)
        ticks_each = instruction_params_dict.get("lineas_cada", 688)

        algorithm = PlotAlgorithmValues.instance_algorithm(instruction_params_dict)

        return cls(algorithm, start_pos, col_window, ticks_each)

    def run(self, X:np.array, groups: list):
        start_pos = self.start_pos if self.start_pos is not None else 0
        col_window = self.col_window if self.col_window is not None else X.shape[0]

        # Run algorithm to get the results
        result_matrix = self.algorithm.result_matrix_in_range(
            X, 
            groups, 
            start_pos,
            col_window
        )

        N = result_matrix.shape[1]

        assert N == col_window

        N_groups = result_matrix.shape[0]
        cummulated_result = np.empty(shape=result_matrix.shape)

        for i in range(N_groups):
            res = 0
            for j in range(N):
                # We want only the values of each row indivually
                res += result_matrix[i,j]
                cummulated_result[i,j] = res

        # Check if values are properly added
        for i in range(N_groups):
            results_per_group = result_matrix[i,:]
            columns_sum = np.sum(results_per_group, axis=0)

            assert np.sum(columns_sum) == cummulated_result[i, -1]


        # Use Dash to plot the results
        external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

        dash_tabs = []
        for i in range(N_groups):
            fig = px.line(x=list(range(start_pos, start_pos + col_window)), y=cummulated_result[i, :])
            fig.add_hline(y=0, line_width=1, line_dash="dash", line_color="red")

            # Add a vertical line each ticks_each
            j = start_pos + self.ticks_each - 1
            while j < start_pos + N:
                fig.add_vline(x=j, line_width=1, line_dash="dash", line_color="green")
                j += self.ticks_each - 1 

            dash_tabs.append(
                dcc.Tab(label=f"{str(groups[i][1])}", children=[
                    dcc.Graph(
                        id=f"example-graph-{i}",
                        figure=fig,
                        style={"height": "80vh"}
                    )
                ])
            )

        app.layout = html.Div(dcc.Tabs(dash_tabs))

        sys.stdout.flush()

        app.run_server(port=int(os.environ["DASH_PORT_TO_USE"]), debug=False)



class PlotAlgorithmValues(PlotInstruction):
    def __init__(self, algorithm: Algorithm):
        self.algorithm = algorithm

    @classmethod
    def from_config(cls, instruction_params_dict):
        algorithm_name = instruction_params_dict["algoritmo"]
        algorithm_params = instruction_params_dict["algoritmo_parametros"]

        import parse

        algorithm_cls = parse.ALGORITMOS[algorithm_name]
        algorithm = algorithm_cls.from_config(algorithm_params)

        return cls(algorithm)

    def run(self, X:np.array, groups: list):
        # Run algorithm to get the results
        result_matrix = self.algorithm.result_matrix(X, groups)
        group_matrix = self.algorithm.group_matrix(X, groups)

        N = result_matrix.shape[1]
        N_groups = result_matrix.shape[0]
        cummulated_result = np.empty(shape=result_matrix.shape)

        for i in range(N_groups):
            res = 0
            for j in range(N):
                res += result_matrix[i,j]
                cummulated_result[i,j] = res

        # Check if values are properly added
        for i in range(N_groups):
            results_per_group = result_matrix[i,:]
            columns_sum = np.sum(results_per_group, axis=0)

            assert np.sum(columns_sum) == cummulated_result[i, -1]


        # Use Dash to plot the results
        external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

        dash_tabs = []
        for i in range(N_groups):
            df = pd.DataFrame.from_dict({
                "group": group_matrix[i,:], 
                "pos": list(range(N)), 
                "data": X, 
                "V": result_matrix[i,:],
                "sum": cummulated_result[i, :]
            })
            dash_tabs.append(
                dcc.Tab(label=f"{str(groups[i][1])}", children=[
                    dash_table.DataTable(
                        data=df.to_dict("records"),
                        page_size=688,
                        columns=[{"name": i, "id": i} for i in df.columns],
                        hidden_columns=["group"],
                        style_data_conditional=[
                            {
                                'if': {
                                    'filter_query': '{group} = 1',
                                    "column_id": "data"
                                },
                                'backgroundColor': 'black',
                                'color': 'white'
                            },
                            {
                                'if': {
                                    'filter_query': '{group} = 0',
                                    "column_id": "data"
                                },
                                'backgroundColor': '#0066ff',
                                'color': 'white'
                            },
                            {
                                'if': {
                                    'filter_query': '{V} >= 0',
                                    'column_id': "V"
                                },
                                'backgroundColor': 'green',
                                'color': 'white'
                            },
                            {
                                'if': {
                                    'filter_query': '{V} < 0',
                                    'column_id': "V"
                                },
                                'backgroundColor': 'red',
                                'color': 'black'
                            },
                        ],
                        style_cell = {
                            "font_size" : "25px"
                        }
                    )
                ])
            )

        app.layout = html.Div(dcc.Tabs(dash_tabs))
        app.run_server(port=int(os.environ["DASH_PORT_TO_USE"]), debug=False)



class RepiteALaPrimera(PlotInstruction):

    def __init__(self, groups_to_search_for: list):
        self.groups_to_search_for = groups_to_search_for


    @classmethod
    def from_config(cls, algorithm_params_dict):
        groups_to_search_for = algorithm_params_dict["grupos"]

        return cls(groups_to_search_for)



    def repeat_at_first_try(self, X:np.array, group: list):
        """
        Args:
            X: Array with the sequence of numbers, shape=(N,)
            group: set of numbers for which the algorithm is performed, shape=(L,)
        """

        N = X.shape[0]
        V = {}

        i = 0
        prev = -np.inf
        while i < N:
            if X[i] in group:
                distance = i - prev
                if distance < np.inf:
                    V[distance] = V.get(distance, 0) + 1
                prev = i

            i+=1

        return V

    def run(self, X:np.array, groups:list):
        """
        Args:
            X: numpy array with the database
            groups: this groups list is ignored
        """
        N = X.shape[0]
        # Params
        groups_to_search_for = self.groups_to_search_for
        num_groups = len(groups_to_search_for)
        computed_dicts = []

        for group in groups_to_search_for:
            G = self.repeat_at_first_try(X, group)
            computed_dicts.append(G)

        # Iterate over the keys of each dict to find the max.
        max_value = -np.inf
        for counter_dict in computed_dicts:
            possible_max = max(counter_dict.keys())
            if max_value < possible_max:
                max_value = possible_max


        # Create the table to show in Dash
        table_dict = {}

        indices = list(range(1, max_value + 1))
        table_dict["Repiten a la:"] = indices

        for i in range(num_groups):
            col = np.empty(shape=max_value)
            col.fill(0)

            for key,value in computed_dicts[i].items():
                col[key-1] = value

            table_dict[str(groups_to_search_for[i])] = col

        # Create the dataframe
        df = pd.DataFrame.from_dict(table_dict)

        # Select only the group columns
        df_only_cols = df[[str(group) for group in groups_to_search_for]]

        cummulated_row_sum = np.empty(shape=max_value)
        cummulated_row_sum.fill(0)

        for index, row_series in df_only_cols.iterrows():
            cummulated_row_sum[index] = np.sum(row_series.to_numpy())

        df["Suma"] = cummulated_row_sum

        # Use Dash to plot the results
        external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

        app.layout = dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict('records')
        )

        app.run_server(port=int(os.environ["DASH_PORT_TO_USE"]), debug=False)


class SubTablesPerDay(PlotInstruction):

    def __init__(self, algorithm: Algorithm, size_page: int):
        self.algorithm = algorithm
        self.size_page = size_page

    @classmethod
    def from_config(cls, instruction_params_dict):
        # Instate the algorithm to use for the subtables
        algorithm = PlotAlgorithmValues.instance_algorithm(instruction_params_dict)
        size_page = instruction_params_dict["salto_pagina"]

        return cls(algorithm, size_page)


    def run(self, X:np.array, groups:list):
        N = X.shape[0]
        # Compute number of cols
        num_pages = math.ceil(N / self.size_page)
        jump_size = self.size_page
        num_rows_to_compute = self.size_page - 20 # Constant specified by Francisco

        values_matrix = np.empty(shape=(num_rows_to_compute, num_pages))
        values_matrix.fill(np.nan)

        for i in range(num_rows_to_compute):
            print(f"Computing for pos: {i}")
            for j in range(num_pages):
                pos = i + j * jump_size

                if pos <= N - 1:
                    values_matrix[i, j] = self.algorithm.stop_at_positive(X, groups, pos)
                else:
                    values_matrix[i, j] = 0

        assert not(np.isnan(values_matrix).any())

        table_dict = {}
        table_dict["N"] = list(range(num_rows_to_compute))

        for col in range(num_pages):
            table_dict[str(col + 1)] = values_matrix[:, col]

        table_dict["Sum"] = np.sum(values_matrix, axis=1)
        suma_columna_de_suma = np.sum(table_dict["Sum"])

        binary_array_cummulated = table_dict["Sum"] >= 0
        ratio = np.sum(binary_array_cummulated)/ binary_array_cummulated.shape[0]

        print(f"Ratio de positivas es: {ratio}")

        print(f"Suma de la columna sumatorio es: {suma_columna_de_suma}")

        # Create the dataframe
        df = pd.DataFrame.from_dict(table_dict)

        # Use Dash to plot the results
        external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

        # Style Conditionals
        style_data_conditional = []

        for col in range(num_pages):
            style_data_conditional.append(
                {
                    'if': {
                        'filter_query': f"{{{col+1}}}" + " >= 0",
                        'column_id': str(col+1)
                    },
                    'backgroundColor': 'green',
                    'color': 'black'
                }
            )

            style_data_conditional.append(
                {
                    'if': {
                        'filter_query': f"{{{col+1}}}" + " < 0",
                        'column_id': str(col+1)
                    },
                    'backgroundColor': 'red',
                    'color': 'white'
                }
            )

        style_data_conditional.append(
            {
                'if': {
                    'filter_query': '{Sum} >= 0',
                    'column_id': 'Sum'
                },
                'backgroundColor': 'green',
                'color': 'black'
            }
        )

        style_data_conditional.append(
            {
                'if': {
                    'filter_query': '{Sum} < 0',
                    'column_id': 'Sum'
                },
                'backgroundColor': 'red',
                'color': 'white'
            }
        )

        app.layout = dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict('records'),
            style_data_conditional=style_data_conditional,
            page_size=num_rows_to_compute
        )


        app.run_server(port=int(os.environ["DASH_PORT_TO_USE"]), debug=False)


class SubTablesPerDayRandom(SubTablesPerDay):

    def run(self, X:np.array, groups:list):
        N = X.shape[0]
        # Compute number of cols
        num_pages = math.ceil(N / self.size_page)
        jump_size = self.size_page
        num_rows_to_compute = self.size_page - 20 # Constant specified by Francisco

        values_matrix = np.empty(shape=(num_rows_to_compute, num_pages))
        values_matrix.fill(np.nan)

        for i in range(num_rows_to_compute):
            print(f"Computing for pos: {i}")
            for j in range(num_pages):
                if j <= 0:
                    pos = i + j * jump_size

                    if pos <= N - 1:
                        values_matrix[i, j] = self.algorithm.stop_at_positive(X, groups, pos)
                    else:
                        values_matrix[i, j] = 0

                # Act randomly
                else:
                    start = j * jump_size
                    end = ((j + 1) * jump_size) - 20 # Constant specified by francisco

                    random_pos = np.random.randint(start, end)

                    if random_pos <= N - 1:
                        values_matrix[i, j] = self.algorithm.stop_at_positive(X, groups, random_pos)
                    else:
                        values_matrix[i, j] = 0


        assert not(np.isnan(values_matrix).any())

        table_dict = {}
        table_dict["N"] = list(range(num_rows_to_compute))

        for col in range(num_pages):
            table_dict[str(col + 1)] = values_matrix[:, col]

        table_dict["Sum"] = np.sum(values_matrix, axis=1)

        # Create the dataframe
        df = pd.DataFrame.from_dict(table_dict)

        # Use Dash to plot the results
        external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

        # Style Conditionals
        style_data_conditional = []

        for col in range(num_pages):
            style_data_conditional.append(
                {
                    'if': {
                        'filter_query': f"{{{col+1}}}" + " >= 0",
                        'column_id': str(col+1)
                    },
                    'backgroundColor': 'green',
                    'color': 'black'
                }
            )

            style_data_conditional.append(
                {
                    'if': {
                        'filter_query': f"{{{col+1}}}" + " < 0",
                        'column_id': str(col+1)
                    },
                    'backgroundColor': 'red',
                    'color': 'white'
                }
            )

        style_data_conditional.append(
            {
                'if': {
                    'filter_query': '{Sum} >= 0',
                    'column_id': 'Sum'
                },
                'backgroundColor': 'green',
                'color': 'black'
            }
        )

        style_data_conditional.append(
            {
                'if': {
                    'filter_query': '{Sum} < 0',
                    'column_id': 'Sum'
                },
                'backgroundColor': 'red',
                'color': 'white'
            }
        )

        app.layout = dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict('records'),
            style_data_conditional=style_data_conditional,
            page_size=num_rows_to_compute
        )


        app.run_server(port=int(os.environ["DASH_PORT_TO_USE"]), debug=False)

class StopAtValueSum(PlotInstruction):

    def __init__(self, algorithm: Algorithm):
        self.algorithm = algorithm

    @classmethod
    def from_config(cls, instruction_params_dict):
        # Instate the algorithm to use for the subtables
        algorithm = PlotAlgorithmValues.instance_algorithm(instruction_params_dict)
        
        return cls(algorithm)
        
    def run(self, X:np.array, groups: list):
        N = X.shape[0] 
        
        res = 0
        for pos in range(N - 50):
            print(f"Computing at pos: {pos}")
            res +=  self.algorithm.stop_at_positive(X, groups, pos)
            
        print(f"Sumatorio acumulado: {res}")

        return res

class SubTables(PlotInstruction):

    def __init__(self, algorithm:Algorithm, start_col:int, end_col:int, save_file_path: str):
        """
        Args:
            algorithm: Algoritm to use for the subtables
            start_col: column to start the subtables (inclusive)
            end_col: column to start the subtables (inclusive)
        """
        self.algorithm = algorithm
        self.start_col = start_col
        self.end_col = end_col
        self.save_file_path = save_file_path


    @classmethod
    def from_config(cls, instruction_params_dict):
        # Instate the algorithm to use for the subtables
        algorithm = PlotAlgorithmValues.instance_algorithm(instruction_params_dict)
        start_col, end_col = tuple(instruction_params_dict["rango"])
        save_file_path = instruction_params_dict.get("guardar_en", "resultados.pickle")

        return cls(algorithm, start_col, end_col, save_file_path)

    def run(self, X:np.array, groups: list):
        results_sorted_by_ratio = self.eval(X, groups)
        # best_column_dict = results_sorted_by_ratio[0]
        path = os.path.join(os.getcwd(), self.save_file_path)

        with open(path, "wb") as f:
            pickle.dump(results_sorted_by_ratio, f, pickle.HIGHEST_PROTOCOL)

        print(f"Guardando resultados en {path}")



    def eval(self, X:np.array, groups: list) -> list:
        """
        This method returns an array reverse ordered with the column_dict with the greatest ratio of
        bases that touch positive in the first position.
        """
        N = X.shape[0] 
        # Params
        start_col = self.start_col
        end_col = self.end_col

        # Create the dictionary with all the binary vectors
        binary_vector_dict = {}
        maximum_counter_dict = {}
        minimum_positive_counter_dict = {}
        minimum_negative_counter_dict = {}
        min_end_value_dict = {}
        

        for window in range(start_col, end_col + 1):
            N_subbases = N - window + 1
            binary_vector_dict[window] = np.empty(shape=N_subbases)
            binary_vector_dict[window].fill(np.nan)

        for col in range(start_col, end_col + 1):
            maximum_counter_dict[col] = {}
            minimum_positive_counter_dict[col] = {}
            minimum_negative_counter_dict[col] = {}
            min_end_value_dict[col] = {}


        for i in range(N):
            # print(f"In iteration: {i}")
            X_sub = X[i:i+end_col]
            N_sub = X_sub.shape[0]
            result_matrix = self.algorithm.result_matrix(X_sub, groups)

            columns_sum = np.sum(result_matrix, axis=0)
            cummulated_result = np.empty(shape=len(columns_sum))

            assert X_sub.shape[0] == len(cummulated_result)

            res = 0
            for j in range(N_sub):
                res += columns_sum[j]
                cummulated_result[j] = res

            assert np.sum(columns_sum) == cummulated_result[-1]

            # Fill the binary vector individually
            for col in range(start_col, end_col + 1):
                # Only fill in the binary vector if condition is fullfilled
                # otherwise index out of range if i > N - col
                if i <= N - col:
                    cummulated_result_sub = cummulated_result[0:col]
                    columns_sum_sub = columns_sum[0:col]

                    assert len(cummulated_result_sub) == col

                    if (cummulated_result_sub > 0).any():
                        binary_vector_dict[col][i] = 1

                        max_for_subbase = np.max(cummulated_result_sub)
                        column_dict = maximum_counter_dict[col]
                        column_dict[max_for_subbase] = column_dict.get(max_for_subbase, 0) + 1 

                        min_for_subbase = np.min(cummulated_result_sub)
                        column_dict = minimum_positive_counter_dict[col]
                        column_dict[min_for_subbase] = column_dict.get(min_for_subbase, 0) + 1
                    else:
                        binary_vector_dict[col][i] = 0

                        res = cummulated_result_sub[-1]

                        assert np.sum(columns_sum_sub) == res

                        column_dict = min_end_value_dict[col]
                        column_dict[res] = column_dict.get(res, 0) + 1

                        min_for_subbase = np.min(cummulated_result_sub)
                        column_dict = minimum_negative_counter_dict[col]
                        column_dict[min_for_subbase] = column_dict.get(min_for_subbase, 0) + 1


        # Compute statistics
        results = {}
        for col in range(start_col, end_col + 1):
            
            assert not(np.isnan(binary_vector_dict[col]).any())

            N_subtables = binary_vector_dict[col].shape[0]

            assert N_subtables == N - col + 1

            num_positive = np.sum(binary_vector_dict[col])
            positive_ratio = num_positive / N_subtables

            # Sort the counter dicts
            ordered_maximum_dict = OrderedDict()
            for key in sorted(maximum_counter_dict[col].keys(), reverse=True):
                ordered_maximum_dict[key] = maximum_counter_dict[col][key]

            ordered_minimum_positive_dict = OrderedDict()
            for key in sorted(minimum_positive_counter_dict[col].keys(), reverse=True):
                ordered_minimum_positive_dict[key] = minimum_positive_counter_dict[col][key]

            ordered_minimum_negative_dict = OrderedDict()
            for key in sorted(minimum_negative_counter_dict[col].keys(), reverse=True):
                ordered_minimum_negative_dict[key] = minimum_negative_counter_dict[col][key]

            results[col] = {
                "Columna": col,
                "N Subbases": N_subtables,
                "Tocan positivo": num_positive,
                "Ratio": positive_ratio,
                "Maximum Dict": ordered_maximum_dict,
                "Minimum Positive Dict": ordered_minimum_positive_dict,
                "Minimum Negative Dict": ordered_minimum_negative_dict,
                "Negative End Value Dict": min_end_value_dict[col]
            }


        # Sort the results dictionary per ratio
        col_ratio_tuples = []
        for col, col_dict in results.items():
            ratio = col_dict["Ratio"]
            col_ratio_tuples.append((col, ratio))

        # Sort the cols by ratio
        col_ratio_tuples_sorted = sorted(
            col_ratio_tuples, 
            key=lambda tuple : tuple[1], 
            reverse=True
        )

        results_sorted_by_ratio = []
        for col, ratio in col_ratio_tuples_sorted:
            results_sorted_by_ratio.append(results[col])


        return results_sorted_by_ratio


    def run_and_show(self, X:np.array, groups: list):

        N = X.shape[0] 
        # Params
        start_col = self.start_col
        end_col = self.end_col

        # Create the dictionary with all the binary vectors
        binary_vector_dict = {}
        maximum_counter_dict = {}
        min_end_value_dict = {}
        

        for window in range(start_col, end_col + 1):
            N_subbases = N - window + 1
            binary_vector_dict[window] = np.empty(shape=N_subbases)
            binary_vector_dict[window].fill(np.nan)

        for col in range(start_col, end_col + 1):
            maximum_counter_dict[col] = {}

        for col in range(start_col, end_col + 1):
            min_end_value_dict[col] = {}

        for i in range(N):
            print(f"In iteration: {i}")
            X_sub = X[i:i+end_col]
            N_sub = X_sub.shape[0]
            result_matrix = self.algorithm.result_matrix(X_sub, groups)

            columns_sum = np.sum(result_matrix, axis=0)
            cummulated_result = np.empty(shape=len(columns_sum))

            assert X_sub.shape[0] == len(cummulated_result)

            res = 0
            for j in range(N_sub):
                res += columns_sum[j]
                cummulated_result[j] = res

            assert np.sum(columns_sum) == cummulated_result[-1]

            # Fill the binary vector individually
            for col in range(start_col, end_col + 1):
                # Only fill in the binary vector if condition is fullfilled
                # otherwise index out of range if i > N - col
                if i <= N - col:
                    cummulated_result_sub = cummulated_result[0:col]
                    columns_sum_sub = columns_sum[0:col]

                    assert len(cummulated_result_sub) == col

                    if (cummulated_result_sub > 0).any():
                        binary_vector_dict[col][i] = 1

                        max_for_subbase = np.max(cummulated_result_sub)

                        # if max_for_subbase
                        column_dict = maximum_counter_dict[col]
                        column_dict[max_for_subbase] = column_dict.get(max_for_subbase, 0) + 1 
                    else:
                        binary_vector_dict[col][i] = 0

                        res = cummulated_result_sub[-1]

                        assert np.sum(columns_sum_sub) == res

                        column_dict = min_end_value_dict[col]
                        column_dict[res] = column_dict.get(res, 0) + 1


        # Compute statistics
        results = {}
        for col in range(start_col, end_col + 1):
            N_subtables = binary_vector_dict[col].shape[0]
            assert N_subtables == N - col + 1

            num_positive = np.sum(binary_vector_dict[col])
            positive_ratio = num_positive / N_subtables

            results[col] = {
                "N Subbases": N_subtables,
                "Tocan positivo": num_positive,
                "Ratio": positive_ratio,
                # "Maximo Comun": maximo_comun
            }

        # Sort the results dictionary per ratio
        col_ratio_tuples = []
        for key, value in results.items():
            ratio = value["Ratio"]
            col_ratio_tuples.append((key, ratio))

        # Sort the cols by ratio
        col_ratio_tuples_sorted = sorted(
            col_ratio_tuples, 
            key=lambda tuple : tuple[1], 
            reverse=True
        )

        sorted_dicts = OrderedDict()
        sorted_maximum_dict = OrderedDict()
        for col, ratio in col_ratio_tuples_sorted:
            sorted_dicts[col] = results[col]
            # sorted_maximum_dict[col] = OrderedDict(sorted(maximum_counter_dict[col].items()))


        # df = pd.DataFrame()
        # # df["Campos"] = ["Nº Subbases", "Tocan Positivo", "Ratio", "Maximo Comun"]
        # df["Campos"] = ["Nº Subbases", "Tocan Positivo", "Ratio"]

        # # for key, value in sorted_dicts.items():
        # for i, key in enumerate(sorted_dicts.keys()):
        #     col_name = f"C_{i+1}:{key}"
        #     df[col_name] = [sorted_dicts[key][k] for k in sorted_dicts[key].keys()]


        # # df.to_csv(index=False, "check.csv")

        # # Use Dash to plot the results
        # external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        # app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

        # dash_tabs = []
        # dash_tabs.append(
        #     dcc.Tab(label="Columnas Ordenadas Por Ratio", children=[
        #         dash_table.DataTable(
        #             id='table',
        #             columns=[{"name": i, "id": i} for i in df.columns],
        #             data=df.to_dict('records')
        #         )
        #     ])
        # )

        # # Iterate over the keys of each dict to find the max.
        # max_value = -np.inf
        # for key, col_dict in sorted_maximum_dict.items():
        #     col_dict = max(col_dict.keys())
        #     if max_value < possible_max:
        #         max_value = possible_max


        # df = pd.DataFrame()
        # df["Valores"] = list(range(1, max_value + 1))

        # for i, key in enumerate(sorted_maximum_dict.keys()):
        #     col_name = f"C_{i+1}:{key}"
        #     count_array = np.zeros(shape=max_value)

        #     for k, v in sorted_maximum_dict[key].items():
        #         count_array[k] = v

        #     df[col_name] = count_array

        # # dash_tabs.append(
        # #     dcc.Tab(label="Valores Maximos Comunes", children=[
        # #     ]
        # # )

        # app.layout = html.Div(dcc.Tabs(dash_tabs))
        # app.run_server(port=int(os.environ["DASH_PORT_TO_USE"]), debug=True)
