import unittest
import numpy as np
import pandas as pd
from algorithm import assing_groups
from algorithm import counting_algorithm

from instructions import conteo_por_salto

class TestCountingAlgorithm(unittest.TestCase):

    def setUp(self):
        df = pd.read_csv("data/test.csv") 
        self.data = df["data"]
        self.data = self.data.to_numpy()
        self.expected_output = df["value"].to_numpy()
        self.values_sequence = [-1, -3, -7, -15, -31]

    def test_counting_algorithm(self):
        B1 = ([2, 4, 6, 8, 10], [1, 3, 5, 7, 9])
        A, B = B1
        group_array = assing_groups(self.data, A, B)
        output = counting_algorithm(self.data, group_array, self.values_sequence)

        equal = np.array_equal(output, self.expected_output)

        self.assertEqual(equal, True)


class TestConteoPorSalto(unittest.TestCase):

    def setUp(self):
        df = pd.read_csv("data/fixed_database.csv")
        self.data = df["data"][80: 80 + 16]
        self.data = self.data.to_numpy()
        self.values = [(90, -18), (72, -18), (84, -24), (90, -30)]

        universe_set = set(list(range(1, 36 +1)))
        positive_group = {1, 3, 5, 7, 9, 12}
        negative_group = universe_set - positive_group
        self.group_array = assing_groups(self.data, negative_group, positive_group)

    def test_a_la_primera(self):
        output = conteo_por_salto(
            self.data, 
            self.group_array, 
            values=self.values, 
            jump=1
        )


        print(output)

        raise NotImplementedError

        expected_output = [
            0, -18, 72, -18, -18, 84, -18, -18, -24, -30, 0, 0, -18, -18, 84, 90, -18, -18, 84, 90, -18, 72, -18, -18, -24, -30, 0, 0, -18, -18, -24, -30
        ]

        assert len(output) == len(expected_output)

        equal = np.array_equal(output, expected_output)

        self.assertEqual(equal, True)


    # def test_a_la_segunda(self):
    #     output = conteo_por_salto(
    #         self.data, 
    #         self.group_array, 
    #         values=self.values, 
    #         jump=2
    #     )

    #     # expected_output = [
    #     #     0, 18, 72, 18, 18, 84, 18, 18, 24, 30, 0, 0, 18, 18, 84, 90, 18, 18, 84, 90, 18, 72, 18, 18, 24, 30, 0, 0, 18, 18, 24, 30
    #     # ]

    #     print(output)

    #     assert len(output) == len(expected_output)

    #     equal = np.array_equal(output, expected_output)

    #     self.assertEqual(equal, True)


    # def test_a_la_tercera(self):
    #     output = conteo_por_salto(
    #         self.data, 
    #         self.group_array, 
    #         values=self.values, 
    #         jump=3
    #     )

    #     expected_output = [
    #         0, 0, 0, 0, 0, 90, 0, 0, -18, -18, -24, 90, 0, 0, 90, 0, 0, 0, 90, 0, 0, 0, 0, 0, -18, -18, -24, 90, 0, 0, -18, -18
    #     ]

    #     assert len(output) == len(expected_output)

    #     equal = np.array_equal(output, expected_output)

    #     self.assertEqual(equal, True)


    # def test_a_la_cuarta(self):
    #     output = conteo_por_salto(
    #         self.data, 
    #         self.group_array, 
    #         values=self.values, 
    #         jump=4
    #     )

    #     expected_output = [
    #         0, 0, 0, 0, 0, 0, 0, 0, 0, -18, -18, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -18, -18, 84, 0, 0, 0, -18
    #     ]

    #     assert len(output) == len(expected_output)

    #     equal = np.array_equal(output, expected_output)

    #     self.assertEqual(equal, True)

    # def test_a_la_segunda_con_cero(self):
    #     df = pd.read_csv("data/fixed_database.csv")
    #     data = df["data"].to_numpy()
    #     data = data[96:96 + 16]

    #     universe_set = set(list(range(1, 36 +1)))
    #     positive_group = {1, 3, 5, 7, 9, 12}
    #     negative_group = universe_set - positive_group
    #     group_array = assing_groups(data, negative_group, positive_group)

    #     output = conteo_por_salto(
    #         data, 
    #         group_array, 
    #         values=self.values, 
    #         jump=2
    #     )

    #     expected_output = [
    #         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -18, -18, -24, -30
    #     ]

    #     assert len(output) == len(expected_output)

    #     equal = np.array_equal(output, expected_output)

    #     self.assertEqual(equal, True)

            

if __name__ == "__main__":
    unittest.main()
