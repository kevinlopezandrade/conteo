"""
Script simple que se usó para limpiar la base de datos de entradas que no
correspondian. En caso de querer usarlo simplemente remplazar 'database.txt'
por el archivo correspondiente.
"""

import pandas as pd


df = pd.read_csv("database.txt", header=None, names=["data"], delimiter="\n")
boolean_array = (df["data"] >= 0) & (df["data"] <=36)
df = df.loc[boolean_array]
df = df.reset_index(drop=True)

df.to_csv("clean_database.csv", index=False)
