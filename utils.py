import numpy as np

def assing_groups(X: np.array, A: list, B: list) -> np.array:
    """
    Elements in the array A will be mapped to 0
    Elements in the array B will be assigned to group 1
    Elements not in A or B will be assigned to group -1
    (i.e just the zero is assigned to this group)

    Args:
        X: Array one-dimensional con los datos, shape=(N, )
        A: Grupo disjunto del grupo B
        B: Grupo disjunto del grupo A

    Returns:
        Vector one-dimensional donde cada G[i] especifica el grupo al que pertenece
        el elemento X[i]. Si G[i] = 1 entonces el X[i] pertence al grupo B, y de la misma
        forma para si G[i] = 0 entonces el X[i] pertenecera al grupo A. En la base de datos
        los elemntos X[i] = 0 (i.e con valor zero) no tienen grupo, o dicho de otra forma
        pertencen un tercer grupo disjunto de A y B, este grupo se representa en el array
        G con el valor -1.
    """
    A = set(A)
    B = set(B)

    assert len(A.intersection(B)) == 0

    N = len(X)

    # Array that saves for each item the group it does belong
    G = np.empty(shape=N, dtype=np.int8)
    G.fill(np.nan)

    for i in range(N):

        if X[i] in A:
            G[i] = 0
        elif X[i] in B:
            G[i] = 1
        else:
            G[i] = -1


    assert not(np.isnan(G).any())

    return G

def fill_zero_until_group(V: np.array, G: np.array, start: int, group_size: int = 6) -> int:
    """
    From start position inclusive it searchs for 6 or group_size exact of the
    same group while setting the all the previous visited values to 0.

    Args:
        X: Vector one-dimensional con los valores de la base de datos de tamanyo N, shape=(N,)
        G: Vector one-dimensional con los grupos a los que pertenece cada X[i], shape=(N,)
        start: Posicion en la que empezar a poner los valores a zero y buscar el grupo exacto de 'group_size'
        group_size: El tamanyo del grupo exacto que se debe encontrar.

    Returns:
        index of the 6th item in the group + 1.
    """
    N = len(G)
    i = start + 1
    num_similar = 1

    while i < N:
        if G[i] == G[i-1] and G[i] != -1:
            num_similar += 1
        else:
            if num_similar == group_size:
                break
            num_similar = 1

        i+= 1


    V[start:i] = 0
    
    return i

def find_similar_groups(G: np.array, start:int, group_size: int = 6, offset: int = 58):
    """
    Busca grupos de tamanyo 'group_size' o mayor en el array de grupos. A partir de ahi



    Args:
        X: Vector one-dimensional con los valores de la base de datos de tamanyo N, shape=(N,)
        G: Vector one-dimensional con los grupos a los que pertenece cada X[i], shape=(N,)
        start: Posicion en la que empezar a poner los valores a zero y buscar el grupo exacto de 'group_size'
        group_size: El tamanyo del grupo exacto que se debe encontrar.

    Returns:
        
    """
    N = len(G)
    end = start + offset - 1
    i = start + 1
    num_similar = 1

    # If end falls outside the valid range
    if end > N - 1:
        end = N - 1

    # It's end + 1 to allow the 'end' index be inside the loop
    while i < end + 1:
        if G[i] == G[i-1] and G[i] != -1:
            num_similar += 1
        else:
            if num_similar == group_size:
                return (num_similar, i)
            elif num_similar > group_size:
                return (num_similar, i)
            else:
                num_similar = 1

        i+= 1

    return num_similar, i
