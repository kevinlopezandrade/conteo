import yaml
import sys
import numpy as np
import pandas as pd
import os
import time

from utils import assing_groups
from algorithms import ConteoPorSalto
from algorithms import ConteoBasico
from algorithms import MetaAlgorithmVariable

from plots import RepiteALaPrimera
from plots import PlotAlgorithmValues
from plots import PlotAlgorithmCummulated
from plots import SubTables
from plots import SubTablesPerDay
from plots import SubTablesPerDayRandom
from plots import StopAtValueSum



ALGORITMOS = {
    "conteo_por_salto": ConteoPorSalto,
    "conteo_basico": ConteoBasico,
    "meta_algoritmo": MetaAlgorithmVariable
}

GRAFICOS = {
    "repite_a_la": RepiteALaPrimera,
    "grafico_valores": PlotAlgorithmValues,
    "grafico_acumulado": PlotAlgorithmCummulated,
    "subbases": SubTables,
    "salto_paginas": SubTablesPerDay,
    "salto_paginas_random": SubTablesPerDayRandom,
    "sumatorio_stop_positivo": StopAtValueSum

}

def read_config_algorithm(config_dict: str):
    data_file = config_dict["base_de_datos"]
    groups_dict = config_dict["grupos"]
    algorithm_name = config_dict["algoritmo"]
    algorithm_params = config_dict.get("algoritmo_parametros", None)

    groups = []
    universe = set(list(range(1, 36 + 1)))
    for key, value in groups_dict.items():
        positive_group = set(value)

        assert len(positive_group) == len(value)

        # The negative group is always the set difference
        negative_group = universe - positive_group

        # Back to lists
        positive_group = sorted(list(positive_group))
        negative_group = sorted(list(negative_group))

        groups.append((negative_group, positive_group))



    algorithm_cls = ALGORITMOS[algorithm_name]
    algorithm = algorithm_cls.from_config(algorithm_params)

    df = pd.read_csv(data_file)
    X = df["data"][0:32]

    result_matrix = algorithm.result_matrix(X, groups)

    return result_matrix

def read_config_plot(config_dict: str):

    data_file = config_dict["base_de_datos"]
    groups_dict = config_dict.get("grupos", None)
    algorithm_name = config_dict["instruccion"]
    algorithm_params = config_dict["instruccion_parametros"]

    if groups_dict is not None:
        groups = []
        universe = set(list(range(1, 36 + 1)))
        for key, value in groups_dict.items():
            positive_group = set(value)

            assert len(positive_group) == len(value)

            # The negative group is always the set difference
            negative_group = universe - positive_group

            # Back to lists
            positive_group = sorted(list(positive_group))
            negative_group = sorted(list(negative_group))

            groups.append((negative_group, positive_group))
    else:
        groups = None


    algorithm_cls = GRAFICOS[algorithm_name]
    algorithm = algorithm_cls.from_config(algorithm_params)

    df = pd.read_csv(data_file)
    X = df["data"]
    X = X.to_numpy()

    algorithm.run(X, groups)

def read_config(config_path: str):
    with open(config_path, "r") as file:
        config_dict = yaml.safe_load(file)

    tipo = config_dict["tipo"]

    if tipo == "algoritmo":
        read_config_algorithm(config_dict)
    elif tipo == "grafico":
        read_config_plot(config_dict)



if __name__ == "__main__":
    config_path = sys.argv[1]

    if len(sys.argv) > 2:
        port_to_use = sys.argv[2]
        os.environ["DASH_PORT_TO_USE"] = port_to_use
    else:
        os.environ["DASH_PORT_TO_USE"] = str(8050)

    start_time = time.time()
    read_config(config_path)
    print(f"---{time.time() - start_time}---")
