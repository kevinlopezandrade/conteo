import numpy as np
import pandas as pd


import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

from collections import OrderedDict



def assing_groups(X: np.array, A: list, B: list) -> np.array:
    """
    Elements in the array A will be mapped to 0
    Elements in the array B will be assigned to group 1
    Elements not in A or B will be assigned to group -1
    (i.e just the zero is assigned to this group)
    """
    A = set(A)
    B = set(B)

    assert len(A.intersection(B)) == 0

    N = len(X)

    # Array that saves for each item the group it does belong
    G = np.empty(shape=N, dtype=np.int8)
    G.fill(np.nan)

    for i in range(N):

        if X[i] in A:
            G[i] = 0
        elif X[i] in B:
            G[i] = 1
        else:
            G[i] = -1


    assert not(np.isnan(G).any())

    return G

def counting_algorithm(X: np.array, G: np.array, values_sequence: list = [-1, -3, -7, -15, -31]) -> np.array:
    """
    The counting algorithm.

    args:
        data: The values of the database
        G: The array that specifies the group of each item with index i
    """

    assert X.shape[0] == G.shape[0] 

    N = X.shape[0]
    N_s = len(values_sequence)

    # Array with values of each item with index i.
    V = np.empty(shape=N)
    V.fill(np.nan)

    # ALGORITHM
    V[0] = 0
    i = 1
    while i < N:
        # If the previous one is zero then my value is zero
        if X[i-1] == 0:
            V[i] = 0
        elif X[i] == 0:
            if V[i-1] > 0:
                V[i] = -0.5
            elif V[i-1] < 0:
                if V[i-1] == values_sequence[-1]:
                    V[i] = -0.5
                else:
                    V[i] = V[i-1] - 0.5
            else:
                V[i] = -0.5
        else:
            if G[i-1] == G[i]:
                # We found the first one
                num_similar = 1
                V[i] = values_sequence[num_similar - 1]

                # Loop again until you really find one that is not in the same group
                # in this way we avoid the issue of using a repeating
                i+=1 
                
                if i == N:
                    break

                while i < N:
                    if G[i-1] == G[i] and num_similar < N_s:
                        num_similar += 1 
                        V[i] = values_sequence[num_similar - 1]
                    else:
                        break
                    i+=1

                if i == N:
                    break

                if num_similar == N_s:
                    # I have finished the sequence so look for the first item that does not have the same group
                    while i < N:
                        if G[i-1] == G[i]:
                            V[i] = 0
                        else:
                            break
                        i+=1

                    if i == N:
                        break

                    # Dos casos o es por que es otro grupo o es por que ha encontrado zero.
                    if X[i] == 0:
                        V[i] = 0
                    else:
                        V[i] = 0

                else:
                    # It corresponds the value that would have get if they were equal
                    if X[i] == 0:
                        # Since I dont' set the value V[i] I continue withou increasing i
                        continue

                    V[i] = -values_sequence[num_similar]
            else:
                V[i] = 1

        i+=1


    # Check if some values have not been properly added to the V.
    assert not(np.isnan(V).any())

    return V


def compute_results(result_matrix: np.array) -> float:
    """
    Given a matrix with the results generated for each binary group,
    first add up the columns and then sum the result along the x axis
    
    Args:
        result_matrix: Matrix with shape (L, N) where N is number of items and
            L the number of binary groups for which the algorithm was ran.
    """
    N =  result_matrix.shape[1]

    # Sum the columns
    columns_sum = np.sum(result_matrix, axis=0)

    assert len(columns_sum) == N

    # Sum all the colum-cummulated values
    total_sum = np.sum(columns_sum)

    return total_sum


def compute_instruction(X: np.array, groups: list, algorithm: str):
    N = X.shape[0]
    n_groups = len(groups)
    result_matrix = np.empty(shape=(n_groups, N))
    group_matrix = np.empty(shape=(n_groups, N))

    for i in range(n_groups):
        A, B = groups[i]
        G = assing_groups(X, A, B)
        group_matrix[i, :] = G
        result_matrix[i, :] = AVAILABLE_ALGORITHMS[algorithm](X, G)

    return result_matrix, group_matrix



def compute_instruction_1(data: pd.Series, disjoint_groups: list):

    data = data.to_numpy()
    N = len(data)
    n_groups = len(disjoint_groups)
    result_matrix = np.empty(shape=(n_groups, N))
    group_matrix = np.empty(shape=(n_groups, N))

    for i in range(n_groups):
        A, B = disjoint_groups[i]
        G = assing_groups(data, A, B)
        group_matrix[i, :] = G
        result_matrix[i, :] = meta_algorithm_variable(data, G)

    return result_matrix, group_matrix

def fill_zero_until_group(V: np.array, G: np.array, start: int, group_size: int = 6) -> int:
    """
    From start position inclusive it searchs for 6 exact of the same group while
    setting the values to 0.


    Returns:
        index of the 6th item in the group + 1.
    """
    N = len(G)
    i = start + 1
    num_similar = 1

    while i < N:
        if G[i] == G[i-1] and G[i] != -1:
            num_similar += 1
        else:
            if num_similar == group_size:
                break
            num_similar = 1

        i+= 1


    V[start:i] = 0
    
    return i

def find_similar_groups(G: np.array, start:int, group_size: int = 6, offset: int = 58):
    N = len(G)
    end = start + offset - 1
    i = start + 1
    num_similar = 1

    # If end falls outside the valid range
    if end > N - 1:
        end = N - 1

    # It's end + 1 to allow the 'end' index be inside the loop
    while i < end + 1:
        if G[i] == G[i-1] and G[i] != -1:
            num_similar += 1
        else:
            if num_similar == group_size:
                return (num_similar, i)
            elif num_similar > group_size:
                return (num_similar, i)
            else:
                num_similar = 1

        i+= 1

    return num_similar, i

def meta_algorithm_variable(
    data: np.array, 
    G: np.array, 
    group_size: int = 6, 
    offset: int = 58
) -> np.array:

    X = data
    N = len(X)
    V = np.empty(shape=N)
    V.fill(np.nan)


    start = 0
    from_just_six = False
    while start < N:
        if not from_just_six:
            end = fill_zero_until_group(V, G, start, group_size=group_size)
            # Value at the first position is always zero unless its the last one
            if end < N:
                print(f"Found group of 6 at: {end}")
                V[end] = 0

            start = end 

            # If start is N we exit the loop since we have finished
            if start == N:
                break

        size_subset, pos = find_similar_groups(G, start, group_size=group_size, offset=offset)

        if size_subset >= group_size:
            if size_subset > group_size:
                print(f"Within +58 of the previous group there is a, 6+ - at {pos}")
                end = pos - (size_subset - group_size)
                V[start:end] = counting_algorithm(X[start:end], G[start:end])
                # Update start position for the next iteration
                start = end
                from_just_six = False
            else:
                print(f"Within +58 of the previous group there is a, 6 - at {pos}")
                end = pos
                V[start:end] = counting_algorithm(X[start:end], G[start:end])

                # Only if end is a valid index
                if end < N:
                    V[end] = 0

                # Update start position for the next iteration
                start = end
                from_just_six = True

        else:
            # Fill the next 58 values with the counting algorithm
            # Recall that [i, j+1) = [i, j] in numpy slicing
            end = pos
            print(f"Within +58 of the previous group there is no group of 6/6+ in between {start}, {pos - 1}")
            V[start:end] = counting_algorithm(X[start:end], G[start:end])


            # MODIFICATION TO THE PREVIOUS ALGORITHM
            last_pos_in_range = end - 1

            if V[last_pos_in_range] < 0 and end < N:
                print("Extending the range")
                # HEre I'm already at l <= n - 2
                # Continue looping
                # start = last_pos_in_range
                end = last_pos_in_range + 2
                res = counting_algorithm(X[start:end], G[start:end])[-1]
                V[start:end] = counting_algorithm(X[start:end], G[start:end])

                # We are at the end of the list
                if end == N:
                    V[start:end] = counting_algorithm(X[start:end], G[start:end])
                    break

                while res <= 0 and end <= N - 1:
                    end += 1
                    res = counting_algorithm(X[start:end], G[start:end])[-1]
                    V[start:end] = counting_algorithm(X[start:end], G[start:end])


                start = end

                if start < N:
                    V[end] = 0

                from_just_six = False
                # # Update start position for the next algorithm 
                # start = end 

            else:
                # Only if end is a valid index
                if end < N:
                    V[end] = 0

                from_just_six = False
                # Update start position for the next algorithm 
                start = end 


    # Check if some values have not been properly added to the V.
    assert not(np.isnan(V).any())

    return V


def meta_algorithm(data: np.array, G: np.array, offset: int = 58) -> np.array:

    X = data
    N = len(X)
    V = np.empty(shape=N)
    V.fill(np.nan)


    start = 0
    from_just_six = False
    while start < N:
        if not from_just_six:
            end = fill_zero_until_6(V, G, start)
            # Value at the first position is always zero unless its the last one
            if end < N:
                V[end] = 0

            start = end 

            # If start is N we exit the loop since we have finished
            if start == N:
                break

        size_subset, pos = find_similar_groups(G, start)

        if size_subset >= 6:
            if size_subset > 6:
                end = pos - (size_subset - 6) 
                V[start:end] = counting_algorithm(X[start:end], G[start:end])
                # Update start position for the next iteration
                start = end
                from_just_six = False
            else:
                end = pos
                V[start:end] = counting_algorithm(X[start:end], G[start:end])

                # Only if end is a valid index
                if end < N:
                    V[end] = 0

                # Update start position for the next iteration
                start = end
                from_just_six = True

        else:
            # Fill the next 58 values with the counting algorithm
            # Recall that [i, j+1) = [i, j] in numpy slicing
            end = pos
            V[start:end] = counting_algorithm(X[start:end], G[start:end])

            # Only if end is a valid index
            if end < N:
                V[end] = 0

            from_just_six = False
            # Update start position for the next algorithm 
            start = end 


    # Check if some values have not been properly added to the V.
    # assert not((V == 99).any())
    assert not(np.isnan(V).any())

    return V



def plot_results_meta_algorithm(X: np.array, group_matrix: np.array, result_matrix: np.array):
    """
    This functions take the result_matrix and computes the vertical columns sum and the final
    sum. Then it plots the results using dash.
    """
    N = result_matrix.shape[1]
    columns_sum = np.sum(result_matrix, axis=0)
    cummulated_result = np.empty(shape=len(columns_sum))

    assert len(columns_sum) == len(cummulated_result)

    res = 0
    for i in range(N):
        res += columns_sum[i]
        cummulated_result[i] = res

    assert np.sum(columns_sum) == res

    # Use Dash to plot the results

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    # Add a vertical line each 688
    fig = px.line(cummulated_result)
    fig.add_hline(y=0, line_width=1, line_dash="dash", line_color="red")

    i = 688 - 1
    while i < N:
        fig.add_vline(x=i, line_width=1, line_dash="dash", line_color="green")
        i += 688 - 1 

    # Create a scatter plot to see the patterns.
    # scatter_fig = px.scatter(x=list(range(100)), y=group_matrix[0,0:100])

    df = pd.DataFrame.from_dict({
        "group": group_matrix[0,:], 
        "pos": list(range(N)), 
        "data": X, 
        "V": result_matrix[0,:],
        "sum": cummulated_result
    })

    app.layout = html.Div([
        dcc.Tabs([
            dcc.Tab(label='Plot', children=[
                dcc.Graph(
                    id='example-graph',
                    figure=fig,
                    style={"height": "80vh"}
                )
            ]),
            # dcc.Tab(label="Patterns", children=[
            #     dcc.Graph(
            #         id="scatter-patterns",
            #         figure=scatter_fig,
            #         style={"height": "80vh"}
            #     )
            # ]),
            dcc.Tab(label='Table', children=[
                dash_table.DataTable(
                    data=df.to_dict("records"),
                    page_size=688,
                    columns=[{"name": i, "id": i} for i in df.columns],
                    hidden_columns=["group"],
                    style_data_conditional=[
                        {
                            'if': {
                                'filter_query': '{group} = 1',
                                "column_id": "data"
                            },
                            'backgroundColor': 'black',
                            'color': 'white'
                        },
                        {
                            'if': {
                                'filter_query': '{group} = 0',
                                "column_id": "data"
                            },
                            'backgroundColor': '#0066ff',
                            'color': 'white'
                        },
                        {
                            'if': {
                                'filter_query': '{V} >= 0',
                                'column_id': "V"
                            },
                            'backgroundColor': 'green',
                            'color': 'white'
                        },
                        {
                            'if': {
                                'filter_query': '{V} < 0',
                                'column_id': "V"
                            },
                            'backgroundColor': 'red',
                            'color': 'black'
                        },
                    ],
                    style_cell = {
                        "font_size" : "25px"
                    }
                )
            ])
        ])
    ])

    app.run_server(debug=False)



def repeat_at_first_try(X:np.array, group: list):
    """
    Args:
        X: Array with the sequence of numbers, shape=(N,)
        group: set of numbers for which the algorithm is performed
    """

    N = X.shape[0]
    G = {}

    i = 0
    prev = -np.inf
    while i < N:
        if X[i] in group:
            distance = i - prev
            if distance < np.inf:
                G[distance] = G.get(distance, 0) + 1
            prev = i

        i+=1

    return G


def plot_repeat_at_first_try(X:np.array, groups: list):
    """
    Args:
        X: Array with the sequence of numbers, shape=(N,)
        groups: List of lists
    """
    N = X.shape[0]
    num_groups = len(groups)
    computed_dicts = []

    for group in groups:
        G = repeat_at_first_try(X, group)
        computed_dicts.append(G)

    # Iterate over the keys of each dict to find the max.
    max_value = -np.inf
    for counter_dict in computed_dicts:
        possible_max = max(counter_dict.keys())
        if max_value < possible_max:
            max_value = possible_max


    # Create the table to show in Dash
    table_dict = {}

    indices = list(range(1, max_value + 1))
    table_dict["Repiten a la:"] = indices

    for i in range(num_groups):
        col = np.empty(shape=max_value)
        col.fill(0)

        for key,value in computed_dicts[i].items():
            col[key-1] = value

        table_dict[str(groups[i])] = col

    # Create the dataframe
    df = pd.DataFrame.from_dict(table_dict)

    # Select only the group columns
    df_only_cols = df[[str(group) for group in groups]]

    cummulated_row_sum = np.empty(shape=max_value)
    cummulated_row_sum.fill(0)

    for index, row_series in df_only_cols.iterrows():
        cummulated_row_sum[index] = np.sum(row_series.to_numpy())

    df["Suma"] = cummulated_row_sum

    # Use Dash to plot the results
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    app.layout = dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
    )

    app.run_server(debug=False)



def subtables(X: np.array, groups: list, columns_range: tuple, algorithm: str):

    N = X.shape[0]
    col_start, col_end = columns_range

    assert col_start < col_end

    # col_end cannot be larger than the number of elements,if equality is
    # achieved then only one subbase is created i.e the whole base
    assert col_end <= N

    results = {}

    for col in range(col_start, col_end + 1):
        print("\nComputing subbases for column: {}".format(col))

        window = col
        N_subtables = N - window + 1

        positive_binary_vector = np.empty(shape=N_subtables)
        positive_binary_vector.fill(np.nan)

        subbases_maximums = []
        maximos_dict = {}
        valores_finales_de_negativos_dict = {}

        for i in range(N_subtables):
            print("Computing algorithm for subtable: {}".format(i), end='\r')
            
            assert (i + window) <= N

            X_sub = X[i:i+window]
            N_sub = X_sub.shape[0]

            assert N_sub == col

            result_matrix, group_matrix = compute_instruction(X_sub, groups, algorithm)

            columns_sum = np.sum(result_matrix, axis=0)
            cummulated_result = np.empty(shape=len(columns_sum))

            assert len(columns_sum) == len(cummulated_result)

            res = 0
            for item in range(N_sub):
                res += columns_sum[item]
                cummulated_result[item] = res


            assert np.sum(columns_sum) == res

            # Check if the cummulated sum has hit positive at some point
            if (cummulated_result > 0).any():
                positive_binary_vector[i] = 1
                max_for_subbase = np.max(cummulated_result)
                maximos_dict[max_for_subbase] = maximos_dict.get(max_for_subbase, 0) + 1

                subbases_maximums.append(max_for_subbase)
            else:
                positive_binary_vector[i] = 0
                valores_finales_de_negativos_dict[res] = valores_finales_de_negativos_dict.get(res, 0) + 1


        # Compute the ratio of subbases that have hit positive
        num_positive = np.sum(positive_binary_vector)
        positive_ratio = num_positive / N_subtables

        assert len(subbases_maximums) == num_positive

        maximo_comun = min(subbases_maximums)
        maximo_mayor = max(subbases_maximums)


        print(maximos_dict)
        print(valores_finales_de_negativos_dict)

        results[col] = {
            "N Subbases": N_subtables,
            "Tocan positivo": num_positive,
            "Ratio": positive_ratio,
            "Maximo Comun": maximo_comun
        }


    # Sort the results dictionary per ratio
    col_ratio_tuples = []
    for key, value in results.items():
        ratio = value["Ratio"]
        col_ratio_tuples.append((key, ratio))

    # Sort the cols by ratio
    col_ratio_tuples_sorted = sorted(
        col_ratio_tuples, 
        key=lambda tuple : tuple[1], 
        reverse=True
    )

    sorted_dicts = OrderedDict()
    for col, ratio in col_ratio_tuples_sorted:
        sorted_dicts[col] = results[col]


    df = pd.DataFrame()
    df["Campos"] = ["Nº Subbases", "Tocan Positivo", "Ratio", "Maximo Comun"]

    # for key, value in sorted_dicts.items():
    for i, key in enumerate(sorted_dicts.keys()):
        col_name = f"C_{i+1}:{key}"
        df[col_name] = [sorted_dicts[key][k] for k in sorted_dicts[key].keys()]


    # Save df to csv
    # df.to_csv("subtable_results_for_basic_algorithm_group_B1.csv", index=False)


    # Use Dash to plot the results
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    app.layout = dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
    )

    app.run_server(debug=False)

AVAILABLE_ALGORITHMS = {
    "meta_algorithm": meta_algorithm,
    "meta_algorithm_variable": meta_algorithm_variable,
    "basic_algorithm": counting_algorithm
}

if __name__ == "__main__":
    df = pd.read_csv("data/fixed_database.csv")
    # df = pd.read_csv("data/test.csv")
    data = df["data"]
    # data = data.to_numpy()[688:688*2]
    data = data.to_numpy()
    # B1 = ([2, 4, 6, 8, 10], [1, 3, 5, 7, 9])
    B1 = ([1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36], [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35])
    # B2 = ([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35], [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36])
    # B3 = ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18], [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])
    # A, B = B1
    # G = assing_groups(data, A, B)
    # meta_algorithm(data.to_numpy(), G)

    # result_matrix, group_matrix = compute_instruction(data, [B1], "basic_algorithm")

    # plot_results_meta_algorithm(data, group_matrix, result_matrix)
    # X = np.array([1, 20, 2, 3, 34, 37, 38, 40, 4, 5, 20, 3, 100, 45, 23, 67, 8, 7, 5, 34, 25, 37, 10, 34, 11, 20])
    # group_1 = []
    # group_2 = []
    # group_3 = []
    # group_4 = []
    # group_5 = []
    # group_6 = []

    # plot_repeat_at_first_try(data, [group_1, group_2, group_3, group_4, group_5, group_6])


    # subtables(data, [B1], (19, 688), "basic_algorithm")
