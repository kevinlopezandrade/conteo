"""
Este modulo implementa una clase 'abstracta' para la creacion de algoritmos de
conteo. Ademas se presentan varias subclases de esta clase principal.
"""
import numpy as np
from utils import assing_groups
from utils import find_similar_groups, fill_zero_until_group


class Algorithm:
    """Clase abstracta para la implementacion de algoritmos de conteo.

    Esta clase, que a la vez tambien actua como interfaz a implementar para sus
    subclases, proporciona una manera facil de implementar algoritmos de conteo
    que REQUIERAN de un vector de valores X y un vector de grupos G a los que
    pertenece cada valor en X (comprobar metodo assing_groups en el modulo
    utils.py). Las subclases que lo implementen solo necesitaran proporcionar
    un metodo:

        def run(self, X:np.array, G:np.array) -> np.array

    que realize las computaciones y devuelva un vector de valores.

    Tambien se proporcionan metodos para el almacenamiento en memoria de los
    resultados obtenidos por un algoritmo aplicado a varios grupos.

    Las subclases tambien deberan implementar un metodo de clase:
        @classmethod
        def from_config(cls, config_dict)

    Que facilitara la creacion del algoritmo a partir de un archivo yaml donde
    se especifican los parametros/atributos que posteriormente seran usados en
    el metodo __init__ del algoritmo implementado. Se sigue a grandes rasgos la
    interfaz disenyada para los estimadores de https://scikit-learn.org/stable/
    donde cada estimador solo actua sobre los datos y los parametros de cada
    estimador son especificados al instanciar el objeto.
    """


    def run(self, X: np.array, G: np.array) -> np.array:
        """
        Metodo a implementar por las subclases. Que asigna a cada elemento X[i]
        un valor V[i], en funcion del grupo G[i] y del algoritmo de conteo
        utilizado.

        Args:
            X: Vector one-dimensional con los valores de la base de datos de tamanyo N, shape=(N,)
            G: Vector one-dimensional con los grupos a los que pertenece cada X[i], shape=(N,)

        Returns:
            Vector one-dimensional 'V' con los valores asignados a cada X[i] por el algoritmo"
        """
        raise NotImplementedError

    def run_in_range(self, X:np.array, G:np.array, start_pos:int, col_window: int) -> np.array:
        """
        Medodo helper que ejecuta 'run' en un un rango indicado por [start_pos, start_pos + col_window - 1]
        inclusive.

        Args:
            X: Vector one-dimensional con los valores de la base de datos de tamanyo N, shape=(N,)
            G: Vector one-dimensional con los grupos a los que pertenece cada X[i], shape=(N,)
            start_pos: Posicion inicial del rango
            col_window: Tamanyo de ventana que se usara en el computo del rango

        Returns:
            Vector one-dimensional 'V' de tamanyo, shape=(col_window,), con
            los valores asignados a cada X[i] por el algoritmo.
        """
        X_sub = X[start_pos:start_pos + col_window]
        G_sub = G[start_pos:start_pos + col_window]
        V_sub = self.run(X_sub, G_sub)

        assert V_sub.shape[0] == col_window

        return V_sub

    def result_matrix(self, X:np.array, groups:list) -> np.array:
        """
        Metodo que computa un algoritmo (i.e el metodo 'run') por cada grupo proporcionado en la
        lista de tuplas de grupos 'groups'.

        Args:
            X: Vector one-dimensional con los valores de la base de datos de tamanyo N, shape=(N,)
            groups: Lista de tuplas en la que cada tupla continue un (grupo, complemento del grupo) donde el
                universo se toma como [0, 36] inclusive. Estas tuplas seran posteriormente usadas por
                'assign_groups' para crear el vector de valores G que sera utilizado por los algoritmos en
                'run'.

        Returns:
            Una matriz de tamanyo GxN donde G=len(groups). shape=(G, N). Cada
            fila G[i:] corresponde al vector V que devuele el metodo 'run'
            habiendo usado el grupo correspondiente.
        """
        N = X.shape[0]
        n_groups = len(groups)
        result_matrix = np.empty(shape=(n_groups, N))

        for i in range(n_groups):
            A, B = groups[i]
            G = assing_groups(X, A, B)
            result_matrix[i, :] = self.run(X, G)

        return result_matrix

    def result_matrix_in_range(self, X:np.array, groups:list, start_pos:int, col_window: int) -> np.array:
        """
        Metodo similar a 'run_in_range' describido anteriormente.
        """
        X_sub = X[start_pos:start_pos + col_window]
        result_matrix_sub = self.result_matrix(X_sub, groups)

        return result_matrix_sub

    def group_matrix(self, X:np.array, groups:list) -> np.array:
        """
        Metodo para obtener los vectores G (i.e pertenencia de grupo) para un conjunto de grupos
        'groups'.

        Args:
            X: Vector one-dimensional con los valores de la base de datos de tamanyo N, shape=(N,)
            groups: Lista de tuplas en la que cada tupla continue un (grupo, complemento del grupo) donde el
                universo se toma como [0, 36] inclusive. Estas tuplas seran posteriormente usadas por
                'assign_groups' para crear el vector de valores G que sera utilizado por los algoritmos en
                'run'.
        Returns:
            Devuelve una matrix de tamanyo GxN G=len(groups) donde cada
            fila G[i:] es el vector de grupo correspondiente para el array X.
        """
        N = X.shape[0]
        n_groups = len(groups)
        group_matrix = np.empty(shape=(n_groups, N))

        for i in range(n_groups):
            A, B = groups[i]
            G = assing_groups(X, A, B)
            group_matrix[i, :] = G

        return group_matrix


    def stop_at_positive(self, X: np.array, groups: list, pos: int) -> float:
        N = X.shape[0]

        increment = 1
        while pos + increment <= N:
            result_matrix = self.result_matrix_in_range(X, groups, pos, increment)
            columns_sum = np.sum(result_matrix, axis=0)
            N_sub = columns_sum.shape[0]

            # Just safecty check for now
            res = 0
            for i in range(N_sub):
                res += columns_sum[i]

            assert res == np.sum(columns_sum)

            res = 0
            for i in range(N_sub):
                if res > 0:
                    return res
                elif res <= -108:
                    return res
                else:
                    res += columns_sum[i]

            increment += 1

        # In the almost impossible case in which neigher goes positive value or
        # falls below -36, return the las value of res
        return res


    @classmethod
    def from_config(cls, algorithm_params_dict):
        """
        Todas las subclases deberan implementar este metodo que debera
        instanciar el objeto a partir de los parametros especificados en
        'algorithm_params_dict'. Valores por defecto para un algoritmo en caso
        de que no se encuentren en el archivo de configuracion yaml deberan ser
        tratados en este metodo.

        Args:
            algorithm_params_dict: Diccionario con los parametros del algoritmo a instanciar

        Returns:
            Objeto algoritmo listo para ser ejecutado.
        """
        raise NotImplementedError


class ConteoBasico(Algorithm):
    """
    Algoritmo de conteo basico.
    La logica y el por que deben ser preguntados a Francisco.
    En esta clase solo se implementaron sus instrucciones.
    """
    
    def __init__(self, values_sequence: list):
        self.values_sequence = values_sequence


    @classmethod
    def from_config(cls, algorithm_params_dict):
        # TODO: Ceck with Francisco the params for this algorithm
        values_sequence = [-1, -3, -7, -15, -31]
        # values_sequence = algorithm_params_dict.get("valores_sequencia", values_sequence)

        return cls(values_sequence)

    def run(self, X:np.array, G:np.array) -> np.array:

        assert X.shape[0] == G.shape[0] 

        # Params
        values_sequence = self.values_sequence

        N = X.shape[0]
        N_s = len(values_sequence)

        # Array with values of each item with index i.
        V = np.empty(shape=N)
        V.fill(np.nan)


        # ALGORITHM
        V[0] = 0
        i = 1
        while i < N:
            # If the previous one is zero then my value is zero
            if X[i-1] == 0:
                V[i] = 0
            elif X[i] == 0:
                if V[i-1] > 0:
                    V[i] = -0.5
                elif V[i-1] < 0:
                    if V[i-1] == values_sequence[-1]:
                        V[i] = -0.5
                    else:
                        V[i] = V[i-1] - 0.5
                else:
                    V[i] = -0.5
            else:
                if G[i-1] == G[i]:
                    # We found the first one
                    num_similar = 1
                    V[i] = values_sequence[num_similar - 1]

                    # Loop again until you really find one that is not in the same group
                    # in this way we avoid the issue of using a repeating
                    i+=1 
                    
                    if i == N:
                        break

                    while i < N:
                        if G[i-1] == G[i] and num_similar < N_s:
                            num_similar += 1 
                            V[i] = values_sequence[num_similar - 1]
                        else:
                            break
                        i+=1

                    if i == N:
                        break

                    if num_similar == N_s:
                        # I have finished the sequence so look for the first item that does not have the same group
                        while i < N:
                            if G[i-1] == G[i]:
                                V[i] = 0
                            else:
                                break
                            i+=1

                        if i == N:
                            break

                        # Dos casos o es por que es otro grupo o es por que ha encontrado zero.
                        if X[i] == 0:
                            V[i] = 0
                        else:
                            V[i] = 0

                    else:
                        # It corresponds the value that would have get if they were equal
                        if X[i] == 0:
                            # Since I dont' set the value V[i] I continue withou increasing i
                            continue

                        V[i] = -values_sequence[num_similar]
                else:
                    V[i] = 1

            i+=1


        # Check if some values have not been properly added to the V.
        assert not(np.isnan(V).any())

        return V


class ConteoPorSalto(Algorithm):
    """
    De nuevo, la logica del algoritmo y el por que de este metodo de asignacion
    de valores deben ser preguntados a Francisco. Esta clase solo proporciona
    la implementacion de su algoritmo de conteo.
    """
    def __init__(self, values:list, jump:int):
        self.values = values
        self.jump = jump

    @classmethod
    def from_config(cls, algorithm_params_dict: dict):
        values = algorithm_params_dict["valores"]
        jump = algorithm_params_dict["salto"]

        return cls(values, jump)

    def run(self, X:np.array, G:np.array) -> np.array:
        N = X.shape[0]
        V = np.empty(shape=N)
        V.fill(np.nan)

        # Params
        values = self.values 
        jump = self.jump

        # We assume that the group positive is always the G[i] == 1
        base_group = 1

        i = 0
        pos_to_apply = np.inf
        while i < N:
            if i == pos_to_apply:
                value = 0
                while i < N and value < len(values):
                    if G[i] == base_group:
                        V[i] = values[value][0]
                        pos_to_apply = i + jump
                        i+=1
                        break
                    else:
                        if X[i] == 0:
                            V[i] = values[value][1]/2
                        else:
                            V[i] = values[value][1]
                        i+=1

                    value+=1

                # We need to set the value for the current i
                # so skip the i+=1 at the end of the loop
                continue

            elif G[i] == base_group:
                pos_to_apply = i + jump
                V[i] = 0
            else:
                V[i] = 0

            i+=1


        assert not(np.isnan(V).any())

        return V


class MetaAlgorithmVariable(Algorithm):
    """
    Una vez mas, la logica del algoritmo y el por que de este metodo de asignacion
    de valores deben ser preguntados a Francisco. Esta clase solo proporciona
    la implementacion de su algoritmo de conteo.

    Esta clase en su metodo 'run' hace uso del algoritmo ConteoBasico. En teoria
    se podria extender para poder usar cualquier algoritmo de conteo que
    implemente la interfaz Algorithm, siguiendo un disenyo parecido al que se
    encuentra en el modulo plots.py.
    """

    def __init__(self, offset:int, group_size:int):
        # TODO: Permitir usar cualquier algoritmo que implemente Algorithm
        self.offset = offset
        self.group_size = group_size

    @classmethod
    def from_config(cls, algorithm_params_dict: dict):
        # TODO: Permitir usar cualquier algoritmo que implemente Algorithm, revisar modulo plots.py
        # para un ejemplo.
        offset = algorithm_params_dict["intervalo"]
        group_size = algorithm_params_dict["grupo_de"]

        return cls(offset, group_size)


    def run(self, X:np.array, G:np.array) -> np.array:
        # Params
        offset = self.offset
        group_size = self.group_size

        N = len(X)
        V = np.empty(shape=N)
        V.fill(np.nan)

        # TODO: This might have to be done in the init
        # Create the counting algorithm
        values_sequence = [-1, -3, -7, -15, -31]
        counting_algorithm = ConteoBasico(values_sequence)

        start = 0
        from_just_six = False
        while start < N:
            if not from_just_six:
                end = fill_zero_until_group(V, G, start, group_size=group_size)
                # Value at the first position is always zero unless its the last one
                if end < N:
                    print(f"Found group of 6 at: {end}")
                    V[end] = 0

                start = end 

                # If start is N we exit the loop since we have finished
                if start == N:
                    break

            size_subset, pos = find_similar_groups(G, start, group_size=group_size, offset=offset)

            if size_subset >= group_size:
                if size_subset > group_size:
                    print(f"Within +58 of the previous group there is a, 6+ - at {pos}")
                    end = pos - (size_subset - group_size)
                    V[start:end] = counting_algorithm.run(X[start:end], G[start:end])
                    # Update start position for the next iteration
                    start = end
                    from_just_six = False
                else:
                    print(f"Within +58 of the previous group there is a, 6 - at {pos}")
                    end = pos
                    V[start:end] = counting_algorithm.run(X[start:end], G[start:end])

                    # Only if end is a valid index
                    if end < N:
                        V[end] = 0

                    # Update start position for the next iteration
                    start = end
                    from_just_six = True

            else:
                # Fill the next 58 values with the counting algorithm
                # Recall that [i, j+1) = [i, j] in numpy slicing
                end = pos
                print(f"Within +58 of the previous group there is no group of 6/6+ in between {start}, {pos - 1}")
                V[start:end] = counting_algorithm.run(X[start:end], G[start:end])


                # MODIFICATION TO THE PREVIOUS ALGORITHM
                last_pos_in_range = end - 1

                if V[last_pos_in_range] < 0 and end < N:
                    print("Extending the range")
                    # HEre I'm already at l <= n - 2
                    # Continue looping
                    # start = last_pos_in_range
                    end = last_pos_in_range + 2
                    res = counting_algorithm.run(X[start:end], G[start:end])[-1]
                    V[start:end] = counting_algorithm.run(X[start:end], G[start:end])

                    # We are at the end of the list
                    if end == N:
                        V[start:end] = counting_algorithm.run(X[start:end], G[start:end])
                        break

                    while res <= 0 and end <= N - 1:
                        end += 1
                        res = counting_algorithm.run(X[start:end], G[start:end])[-1]
                        V[start:end] = counting_algorithm.run(X[start:end], G[start:end])


                    start = end

                    if start < N:
                        V[end] = 0

                    from_just_six = False
                    # # Update start position for the next algorithm 
                    # start = end 

                else:
                    # Only if end is a valid index
                    if end < N:
                        V[end] = 0

                    from_just_six = False
                    # Update start position for the next algorithm 
                    start = end 


        # Check if some values have not been properly added to the V.
        assert not(np.isnan(V).any())

        return V

class MetaAlgorithm(Algorithm):
    """
    Una vez mas, la logica del algoritmo y el por que de este metodo de asignacion
    de valores deben ser preguntados a Francisco. Esta clase solo proporciona
    la implementacion de su algoritmo de conteo.

    Esta clase en su metodo 'run' hace uso del algoritmo ConteoBasico. En teoria
    se podria extender para poder usar cualquier algoritmo de conteo que
    implemente la interfaz Algorithm, siguiendo un disenyo parecido al que se
    encuentra en el modulo plots.py.
    """

    def __init__(self):
        """Nothgin to do in the init"""
        pass

    @classmethod
    def from_config(cls, algorithm_params_dict):
        return cls()

    def run(self, X: np.array, G: np.array) -> np.array:

        N = len(X)
        V = np.empty(shape=N)
        V.fill(np.nan)

        # TODO: This might have to be done in the init
        # Create the counting algorithm
        values_sequence = [-1, -3, -7, -15, -31]
        counting_algorithm = ConteoBasico(values_sequence)

        start = 0
        from_just_six = False
        while start < N:
            if not from_just_six:
                end = fill_zero_until_group(V, G, start)
                # Value at the first position is always zero unless its the last one
                if end < N:
                    V[end] = 0

                start = end 

                # If start is N we exit the loop since we have finished
                if start == N:
                    break

            size_subset, pos = find_similar_groups(G, start)

            if size_subset >= 6:
                if size_subset > 6:
                    end = pos - (size_subset - 6) 
                    V[start:end] = counting_algorithm.run(X[start:end], G[start:end])
                    # Update start position for the next iteration
                    start = end
                    from_just_six = False
                else:
                    end = pos
                    V[start:end] = counting_algorithm.run(X[start:end], G[start:end])

                    # Only if end is a valid index
                    if end < N:
                        V[end] = 0

                    # Update start position for the next iteration
                    start = end
                    from_just_six = True

            else:
                # Fill the next 58 values with the counting algorithm
                # Recall that [i, j+1) = [i, j] in numpy slicing
                end = pos
                V[start:end] = counting_algorithm.run(X[start:end], G[start:end])

                # Only if end is a valid index
                if end < N:
                    V[end] = 0

                from_just_six = False
                # Update start position for the next algorithm 
                start = end 


        # Check if some values have not been properly added to the V.
        # assert not((V == 99).any())
        assert not(np.isnan(V).any())

        return V
