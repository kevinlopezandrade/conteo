import os
import sys
import yaml
import pickle
import numpy as np
import pandas as pd
import time

from ray import tune
from plots import SubTables
from datetime import datetime
from algorithms import ConteoPorSalto

class HyerOptConteoPorSalto:

    def __init__(
        self,
        X:np.array,
        groups:list,
        start_col:int,
        end_col: int,
        values:int,
        max_jump:int
    ):
        self.X = X
        self.groups = groups
        self.start_col = start_col
        self.end_col = end_col
        self.values = values
        self.max_jump = max_jump

    @classmethod
    def from_config(cls, config_path):
        with open(config_path, "r") as file:
            config_dict = yaml.safe_load(file)

        data_file = config_dict["base_de_datos"]
        df = pd.read_csv(data_file)
        groups_dict = config_dict.get("grupos", None)

        groups = []
        universe = set(list(range(1, 36 + 1)))
        for key, value in groups_dict.items():
            positive_group = set(value)

            assert len(positive_group) == len(value)

            # The negative group is always the set difference
            negative_group = universe - positive_group

            # Back to lists
            positive_group = sorted(list(positive_group))
            negative_group = sorted(list(negative_group))

            groups.append((negative_group, positive_group))


        X = df["data"].to_numpy()
        start_col, end_col = config_dict["rango"]
        params = config_dict["parametros_a_optimizar"]
        values = params["valores"]
        max_jump = params["salto_maximo"]

        return cls(X, groups, start_col, end_col, values, max_jump)


    def run(self):
        X = self.X
        groups = self.groups
        start_col = self.start_col
        end_col = self.end_col
        values = self.values
        max_jump = self.max_jump

        values_to_search = [values[:i] for i in range(1, len(values) + 1)]
        values_index = list(range(len(values_to_search)))
        jumps = list(range(1, max_jump+1))

        search_space = {
            "values_index": tune.grid_search(values_index),
            "jumps": tune.grid_search(jumps) 
        }

        def trainable(config, checkpoint_dir=None):
            values_index = config["values_index"]
            values = values_to_search[values_index]
            jump = config["jumps"]

            algorithm = ConteoPorSalto(values, jump)
            subtables = SubTables(algorithm, start_col, end_col)

            sorted_columns = subtables.eval(X, groups)
            ratio = sorted_columns[0]["Ratio"]

            # Step is always set to 1 since we just want to save the list of
            # ordered dicts

            with tune.checkpoint_dir(step=1) as checkpoint_dir:
                path = os.path.join(checkpoint_dir, "sorted_columns.pickle")
                with open(path, "wb") as f:
                    pickle.dump(sorted_columns, f, pickle.HIGHEST_PROTOCOL)


            tune.report(ratio=ratio)


        # Setup a name for the experiment 
        now = datetime.now()
        date = now.strftime("%d-%m-%Y_%H:%M:%S")
        cls_name = self.__class__.__name__
        exp_name = f"{cls_name}_{date}"

        tune.run(
            trainable,
            name=exp_name,
            config=search_space,
            metric="ratio",
            mode="max",
            num_samples=1,
            local_dir="./ray_results",
            log_to_file=True
        )


if __name__ == "__main__":
    config_file = sys.argv[1]
    hyperopt = HyerOptConteoPorSalto.from_config(config_file)

    start_time = time.time()
    hyperopt.run()
    print(f"---{time.time() - start_time}---")
