import json
import sys
import pickle

if __name__ == "__main__":
    file_path = sys.argv[1]
    with open(file_path, "rb") as f:
        results_sorted_by_ratio = pickle.load(f)

    best_column = results_sorted_by_ratio[0]
    maximum_dict = best_column["Maximum Dict"]
    minimum_positive_dict = best_column["Minimum Positive Dict"]
    minimum_negative_dict = best_column["Minimum Negative Dict"]


    res = 0
    for key, value in minimum_negative_dict.items():
        res += value

    assert res == (best_column["N Subbases"] - best_column["Tocan positivo"])

    res = 0
    for key, value in maximum_dict.items():
        res += value

    assert res == best_column["Tocan positivo"]

    res = 0
    for key, value in minimum_positive_dict.items():
        res += value

    assert res == best_column["Tocan positivo"]


    assert len(set(maximum_dict.keys()).intersection(set(minimum_positive_dict.keys()))) == 0

    with open("positive_maximum.json", "w") as f:
        json.dump(
            maximum_dict, 
            f,
            indent=4,
            separators=(',', ': ')
        )

    with open("positive_minimum.json", "w") as f:
        json.dump(
            minimum_positive_dict, 
            f,
            indent=4,
            separators=(',', ': ')
        )

    with open("negative_minimum.json", "w") as f:
        json.dump(
            minimum_negative_dict, 
            f,
            indent=4,
            separators=(',', ': ')
        )
